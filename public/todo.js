(function($, global) {

  $('#newtodoactivity-bt').click(function(){
    newActivity(this.href + '&hide_parent=true&no_help=true');
    return false;
  });

  function newActivity(url) {
    $('#todo-activities > strong').remove();
    var li = $('<li class="loading">'+url+'</li>').appendTo('#todo-activities');
    $.ajax(url, {
      error: function(xhr, status, msg) {
        li.removeClass('loading').addClass('fail').text('error: '+msg);
      },
      success: function(data, status, xhr) {
        li.removeClass('loading').html(data);
      }
    });
  }



})(jQuery, window);
