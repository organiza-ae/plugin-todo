class TodoPlugin::Activity < Article

  def self.type_name
    _('ToDo Activity')
  end

  def self.short_description
    _('An activity to get done.')
  end

  def self.description
    _('An activity to get done. Set attributes, describe and comment.')
  end

  def self.icon_name(article = nil)
    'todoactivity'
  end

  def accept_comments?
    true
  end

  validates_presence_of :parent, message: _('You must create an activity inside a ToDo list.')

  before_save do |activity|
    puts '-'*88, activity.parent.class, (activity.parent.kind_of? TodoPlugin::TodoList), '+'*88
    unless activity.parent.kind_of? TodoPlugin::TodoList
      activity.parent = nil
      throw _('You must create an activity inside a ToDo list. (Was %s)') % activity.parent.class.name
    end
  end

end
