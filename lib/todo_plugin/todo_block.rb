class TodoPlugin::TodoBlock < Block

  settings_items :list_id, :type => :integer, :default => 0

  def self.description
    _('ToDo List')
  end

  def help
    _('List activities to get Done.')
  end

  def default_title
    _('ToDo List')
  end

  def have_lists?
    !owner.todo_lists.empty?
  end

  def activities
    if list_id == 0
      lists = owner.todo_lists.map &:id
    else
      lists = [list_id]
      #list = TodoPlugin::TodoList.find list_id
      #list.activities
    end
    TodoPlugin::Activity.where(parent_id: lists).order 'published_at DESC'
  end

  def content(args={})
    block = self
    proc do
      if block.have_lists?
        render :file => 'todo_plugin/block_list', :locals => { :block => block }
      else
        name = block.owner == user ? _('You') : block.owner.short_name
        p = block.owner.identifier
        [
          _("%s have no ToDo Lists.") % name,
          button( :todo, s_('ToDo-List|Create one'),
                  controller: :cms, profile: p, action: :new, type: TodoPlugin::TodoList,
                  article: { name: _('ToDo List'), published: false, show_to_followers: false })
        ].join "\n"
      end
    end
  end

  def ctrl_url(action, options={})
    TodoPlugin.myprofile_ctrl_url self.owner, action, options
  end

  def footer
    block = self
    proc do
      if block.have_lists?
        [
          button( :todolist, s_('contents|See All'), block.ctrl_url('all_activities') ),
          modal_button( :newtodoactivity, _('New Activity'), block.ctrl_url('new_activity', list: block.list_id) )
        ].join "\n"
      end.to_s
    end
  end

end
