class TodoPlugin::TodoList < Folder

  has_many :activities, {
    class_name: 'TodoPlugin::Activity',
    foreign_key: 'parent_id',
    source: :children,
    order: 'published_at DESC, id DESC'
  }

  def self.type_name
    _('ToDo List')
  end

  def self.short_description
    _('A list of activities to get done.')
  end

  def self.description
    _('Manange a to-do list. Resgister activities to get done. Import and export as <code>Todo.txt</code>.')
  end

  def self.icon_name(article = nil)
    'todolist'
  end

  def accept_comments?
    true
  end

  def to_html(options = {})
    list = self
    proc do
      render :file => 'todo_plugin/todo_list', :locals => { :list => list }
    end
  end

  def accept_uploads?
    false
  end

  def allow_spread?(user)
    false
  end

  def translatable?
    false
  end

end
