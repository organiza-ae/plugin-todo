class TodoPlugin < Noosfero::Plugin

  TodoPlugin::TodoList # force load

  def self.plugin_name
    "ToDo List"
  end

  def self.plugin_description
    _("Manage to-do lists for people an groups.")
  end

  def stylesheet?
    true
  end

  def content_remove_new(content)
    content.class == TodoPlugin::TodoList
  end

  def self.extra_blocks
    { TodoPlugin::TodoBlock => {} }
  end

  def self.ctrl_url(ctrl, action, options={})
    {
      controller: "todo_plugin_#{ctrl}",
      action: action,
    }.merge options
  end

  def self.myprofile_ctrl_url(profile, action, options={})
    options = options.merge profile: profile.identifier
    self.ctrl_url :myprofile, action, options
  end

  def content_types
    [ TodoPlugin::TodoList ]
  end

  def control_panel_buttons
    title = context.profile.class == Person ? _('My ToDo') : _("%s's ToDo")
    p = context.profile.identifier
    {
      title: title % context.profile.short_name,
      icon: 'todolist',
      url: { controller: 'todo_plugin_profile', action: 'all', profile: p }
    }
  end

end
