require_dependency 'profile'

class Profile

  has_many :todo_lists, :source => 'articles', :class_name => 'TodoPlugin::TodoList'

end
