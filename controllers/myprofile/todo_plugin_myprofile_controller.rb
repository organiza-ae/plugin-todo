class TodoPluginMyprofileController < PublicController

  needs_profile

  def all_activities
    render text: 'ok'
  end

  def new_activity
    list = if params['list'].blank? || params['list'].to_i == 0
      profile.todo_lists.first
    else
      TodoPlugin::TodoList.find params['list']
    end
    if request.post?
      @activity = TodoPlugin::Activity.new params['activity']
      puts '='*88, Article.find(params['list_id']), '+'*88
      @activity.parent = Article.find params['list_id']
      @activity.profile = profile
      @activity.author = user
      @activity.last_changed_by = user
      @activity.created_by = user
      if @activity.save
        render text: @activity.inspect
      else
        render file: 'todo_plugin/activity_create', locals: { list: list }
      end
#      activity = TodoPlugin::Activity.create params['activity']
#      a.errors.full_message
#      if request.xhr?
#        render json: { activity: activity.to_json, ok: true }
#      else
#        redirect_to parent.url
#      end
    else
      @activity = TodoPlugin::Activity.new
      render file: 'todo_plugin/activity_create', locals: { list: list }
    end
  end

end
